Hi there, 

Here is my selenium framework for the interview exercise. Please read carefully before use. 

REQUIREMENTS :

1. JDK 1.7 or 1.8
2. Download ANT from https://ant.apache.org/bindownload.cgi


WARNING : 

1. My linux laptop has crashed so I have wrote the code based on Windows. 
2. I have intentionally restricted the framework to support only Chrome and not IE and FF.
3. I have intentionally not handled default values for properties in selenium.properties when not available.
4. I have intentionally setup the "junit" target not to run specific tests instead all of them. 

INSTRUCTIONS TO BUILD : 

1. I will be using the term PROJECT_ROOT , which is the parent folder "InfiniteCampusTest". 
2. Move the executable chromedriver.exe to a location of your choice. Make sure there is read permissions for this folder.
2. After installing ant , open command shell and go to PROJECT_ROOT/src_selenium . 
3. Run the ant target "compile" to compile the source files and tests. 
		Example :  C:\InfiniteCampusTest\src_selenium> ant compile
	This wil create separate folders {PROJECT_ROOT}/build/classes to be used by runtime.	
4. If you are running tests the first time, the following properties have to be updated in 
	
	{PROJECT_ROOT}/build/classes/com/ic/utils/selenium.properties
	
	4.1 Add the full directory path for the parent folder where executable is located in step 2.
		This path should be added to the property "chrome.plugin.dir"
	
	Example : If chromedriver.exe is dropped in folder D:\Test then the property should be 
					
					chrome.plugin.dir=D:\\Test
				
				Warning : Remember to escape '\' with '\\'
	
	4.2 Set the location where you need the snapshots be saved in "snapshot.dir"
						
	Example : snapshot.dir=D:\\Test\InfiniteCampustTesting\\Results					

5. Now you're all set ! Go back to the command prompt and run the target junit and you will notice the browsers does the testing for you.
		Example  : C:\InfiniteCampusTest\src_selenium> ant junit


IF YOU ARE A DEVELOPER KEEP READING : 

1. This is non-patented code. Feel free to fork it and make your changes. Should you have questions , email sabarish.narain@gmail.com
2. To import to IDE, I have included the .classpath file that will allow to import with ease. All jars within {PROJECT_ROOT}/lib folder have to be directly referenced from your IDE. 
2. To create your own selenium by extending AbstractSeleniumTest and use the build file to compile and run them. 
3. Remember target "junit" will not recompile, this means, if you have modified selenium.properties in build/classes folder, running the "compile" target will overwrite the properties again. 
	AbstractSeleniumTest is the parent test class that needs to be extended by every junit test. It supports all browser handling APIs with abstraction.
4. I have purposefully not coded to extend JunitTestRunner to do the teardown job such as close all browser sessions after test finishes and so on. For now this is done via  AbstractSeleniumTest. 
5. Naming conventions used for classes -
	IC - InfiniteCampus
	PO - PageObject

