package com.ic.exceptions;

public class ICSeleniumException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;

	public ICSeleniumException(String message) {
		super(message);
	}

}
