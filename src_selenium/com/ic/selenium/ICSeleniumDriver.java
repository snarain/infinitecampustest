package com.ic.selenium;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.ic.exceptions.ICSeleniumException;
import com.ic.page.objects.common.Xpath;

public class ICSeleniumDriver {

	static WebDriver wd = null;
	static ChromeOptions options = null;
	static Properties props = null;

	static {


		try {

			props = com.ic.utils.PropertiesHelper.loadProperties("com/ic/utils/selenium.properties");

		} catch (IOException e) {
			throw new ICSeleniumException("Unable to find selenium.properties");
		}

		String chromeExec = props.getProperty("chrome.plugin.dir");
		System.setProperty("webdriver.chrome.driver", chromeExec+"\\chromedriver.exe");

		options = new ChromeOptions();
		options.addArguments("--start-maximized");
	}

	public WebDriver initializeDriver() {
		wd = new ChromeDriver(options);
		return wd;
	}

	public static WebDriver getDriver() {
		return wd;
	}

	public static Properties getProperties() {
		return props;
	}

	public WebElement findElementByID(String id) {
		WebElement e;
		try {
			e = getDriver().findElement(By.id(id));
		}
		catch(NoSuchElementException ex) {
			e= null;
		}

		return e;
	}

	public List<WebElement> findElementsByXpath(String xpath) {
		List<WebElement> lstElems;
		try {
			lstElems = getDriver().findElements(By.xpath(xpath));
		}
		catch(NoSuchElementException ex) {
			lstElems = new ArrayList<WebElement>();
			throw new ICSeleniumException("Element not found " + xpath);
		}

		return lstElems;
	}


	public WebElement findElementByXpath(Xpath xpath) {
		WebElement e;
		try {
			e = getDriver().findElement(By.xpath(xpath.getXpathStr()));
		}
		catch(NoSuchElementException ex) {
			e= null;
		}

		return e;
	}




}
