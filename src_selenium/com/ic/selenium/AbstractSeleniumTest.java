package com.ic.selenium;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.ic.exceptions.ICSeleniumException;

public class AbstractSeleniumTest {
	final String url ="http://localhost:8080/TicketingSystem";
	
	public void start() {
		WebDriver wd = new ICSeleniumDriver().initializeDriver();
		wd.get(url);
	}
	
	public void start(String url) {
		WebDriver wd = new ICSeleniumDriver().initializeDriver();
		wd.get(url);
	}
	
	public String getUrl() {
		return ICSeleniumDriver.getDriver().getCurrentUrl();
	}
	
	public void snapPicture(){
		
		String destFilePath = ICSeleniumDriver.getProperties().getProperty("snapshot.dir");
		
		File snapshot = ((TakesScreenshot)ICSeleniumDriver.getDriver()).getScreenshotAs(OutputType.FILE);
		
		File destdir = new File(destFilePath);

		try {
			
			if(!destdir.exists()) {
				destdir.mkdir();
			}
			
			File destFile = new File(destdir + File.separator + snapshot.getName());
			
			FileUtils.copyFile(snapshot, destFile);
		} catch (IOException e) {
			throw new ICSeleniumException("Unable to capture snapshot" + e);
		}
	}
	
	
	@After
	public void killBrowsers() {
		WebDriver wd = ICSeleniumDriver.getDriver();

		List<String> windowsHandles = new ArrayList<String>(ICSeleniumDriver.getDriver().getWindowHandles());

		if(windowsHandles.size() <= 1) {
			wd.close();
		}
		else {
			
			for(int i=0; i <windowsHandles.size(); i++) {
				String currWindow = windowsHandles.get(i);
				wd.switchTo().window(currWindow).close();
			}
		}
		
		
	}
	
	
}
