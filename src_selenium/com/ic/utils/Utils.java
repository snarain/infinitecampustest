package com.ic.utils;

public class Utils {
	
	
	public static String enclose(String value) {
		return "\'" + value + "\'";
	}

	public static String escape(String s) {
		
		if(s.contains("\\")) {
			s = s.replace("\\", "\\\\");
		}
		return s;
		
	}
}
