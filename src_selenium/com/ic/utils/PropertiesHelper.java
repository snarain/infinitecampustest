package com.ic.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesHelper {

	
	public static Properties loadProperties(String _pathToFile) throws FileNotFoundException, IOException {
		
		Properties props = new Properties();
		InputStream fis = PropertiesHelper.class.getClassLoader().getResourceAsStream(_pathToFile);
		props.load(fis);
		return props;
		
	}
}
