package com.ic.tests.job.openings;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;

import com.ic.page.CareersPagePO;
import com.ic.page.third.party.sites.GoogleSearchPO;
import com.ic.page.third.party.sites.GoogleSearchResultsPO;
import com.ic.page.third.party.sites.TaleoResultsPO;
import com.ic.selenium.AbstractSeleniumTest;

public class JobOpeningsSeleniumTest extends AbstractSeleniumTest{
	
	
	/**
	 * Test Code for test purposes only
	 */
	public void testTest() {
		
		start("https://www.google.com");
		snapPicture();
	}
	
	/**
	 * Verify if Inifinite careers site is the first result in google search for 
	 * search term "infinite campus careers"
	 */
	@Test
	public void testGoogleResults() {
		
		String infiniteCampusCareersResultsTitle = "Careers � Infinite Campus";
		
		start("https://www.google.com");
		GoogleSearchPO search = new GoogleSearchPO();
		GoogleSearchResultsPO grs = search.search("infinite campus careers");
		
		int resIndex = grs.getResultsIndexForTitle(infiniteCampusCareersResultsTitle);
		snapPicture();
		assertThat(resIndex, is(1));
		
		// verify if url is https://www.infinitecampus.com/company/careers
		grs.clickLink(infiniteCampusCareersResultsTitle);
		assertThat(getUrl(), is("https://www.infinitecampus.com/company/careers"));
	}

	/**
	 * Test to validate if Infinite Campus careers site has 3 View open positions href to taleo.
	 */
	@Test

	public void testViewPositionsHref() {
		
		/**
		 * It's ok to start from careers website since we already tested the google search results in previous test. 
		 * 
		 */
		start("https://www.infinitecampus.com/company/careers");
		CareersPagePO careers = new CareersPagePO();
		snapPicture();
		
		int viewPosBtnCount = careers.getViewPositionsButtonsCount();
		assertThat(viewPosBtnCount, is(3));
		
	}
	
	/**
	 * Test to validate if View open positions redirects to taleo.
	 */
	@Test

	public void testViewPositionsFromTaleo() {
		
		/**
		 * It's ok to start from careers website since we already tested the google search results in previous test. 
		 * 
		 */
		start("https://www.infinitecampus.com/company/careers");
		CareersPagePO careers = new CareersPagePO();
		
		TaleoResultsPO taleoRes = careers.clickViewPositions();
		snapPicture();

		int openings = taleoRes.getJobListingCount();
		assertThat(openings, is(10)); // Openings is 10 as of 04/05
		assertThat("Opening unavailable. An test engineer is hired ??",
				taleoRes.isOpeningAvailable("Software Engineer - Test Automation"), is(true));

		
	}

}
