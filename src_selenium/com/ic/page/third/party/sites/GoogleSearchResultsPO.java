package com.ic.page.third.party.sites;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.ic.page.objects.common.Xpath;
import com.ic.page.objects.common.elements.AnchorPO;
import com.ic.page.objects.common.elements.FormPO;

public class GoogleSearchResultsPO extends FormPO{

	public int getResultsIndexForUrl(String url) {
		return 0;
	}

	public int getResultsIndexForTitle(String title) {
		
		List<WebElement> lstElements = findElementsByXpath("//div[@class=\'rc\']/h3/a");
		
		int index = 0;
		
		for(Iterator<WebElement> itr = lstElements.iterator(); itr.hasNext();) {
			
			index++;
			
			WebElement we = itr.next();
			
			if(we.getText().equalsIgnoreCase(title)) {
				return index;
			}
		}
		
		return 0;
		

	}

	public boolean isResultsExistsWithExactTitle(String title) {
		AnchorPO linkPO = new AnchorPO(title);
		return linkPO.isExists();
		
	}
	
	public void clickLink(String title) {
		AnchorPO linkPO = new AnchorPO(title);
		linkPO.click();
	}


}
