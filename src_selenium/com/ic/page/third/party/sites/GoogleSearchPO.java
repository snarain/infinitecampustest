package com.ic.page.third.party.sites;

import org.openqa.selenium.ElementNotVisibleException;

import com.ic.exceptions.ICSeleniumException;
import com.ic.page.objects.common.Xpath;
import com.ic.page.objects.common.elements.ButtonPO;
import com.ic.page.objects.common.elements.TextBoxPO;

public class GoogleSearchPO {
	
	public GoogleSearchPO() {
		
	}
	
	public void enterSearchTerms(String _searchTerms) {
		TextBoxPO searchBox = new TextBoxPO("lst-ib");
		searchBox.enterText(_searchTerms);
	}
	
	public GoogleSearchResultsPO clickSearch() {
		ButtonPO searchBtn = new ButtonPO(new Xpath("input", "value", "Google Search"));
		
		try {
				searchBtn.click();
			}
			catch(ElementNotVisibleException envse) {
				
				// you might already be in results page when search terms are typed. No big deal ! look for maginifier button instead

				ButtonPO magBtn = new ButtonPO(new Xpath("button", "value", "Search"));
				
				if(magBtn.isExists()) {
					magBtn.click();
				}
				else {
					throw new ICSeleniumException("Unable to find Google search button.");
				}
				
			}
			
		return new GoogleSearchResultsPO();

	}
	
	public GoogleSearchResultsPO search(String _searchTerms) {
		enterSearchTerms(_searchTerms);
		clickSearch();
		
		return new GoogleSearchResultsPO();

	}

}
