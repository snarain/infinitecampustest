package com.ic.page.third.party.sites;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.ic.page.objects.common.elements.AnchorPO;
import com.ic.page.objects.common.elements.FormPO;
import com.ic.utils.Utils;

public class TaleoResultsPO extends FormPO {

	
	public int getJobListingCount() {
		
		String jobListsXpath = "//td[@class=" + Utils.enclose("left top") + "]/b/a" ;
		List<WebElement> lstEls = findElementsByXpath(jobListsXpath);
		
		return lstEls.size();
	}
	
	public boolean isOpeningAvailable(String opening) {
		AnchorPO anchor = new AnchorPO(opening);
		return anchor.isExists();
		
	}
	
}
