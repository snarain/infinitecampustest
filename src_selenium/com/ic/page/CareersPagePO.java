package com.ic.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.ic.page.objects.common.elements.AnchorPO;
import com.ic.page.objects.common.elements.FormPO;
import com.ic.page.third.party.sites.TaleoResultsPO;
import com.ic.utils.Utils;

public class CareersPagePO extends FormPO{

	public boolean isViewPositionsButtonPresent() {
		AnchorPO viewpos = new AnchorPO("View Open Positions");
		return viewpos.isExists();
	}
	
	public int getViewPositionsButtonsCount() {
		
		List<WebElement> lstElems = findElementsByXpath("//a[normalize-space(text())=" + Utils.enclose("View Open Positions") + "]");
		return lstElems.size();
	}
	
	
	public TaleoResultsPO clickViewPositions() {
		AnchorPO viewpos = new AnchorPO("View Open Positions");
		viewpos.click();
		
		// target for view positions button is new tab. We need to switch browser handler to new tab.
		
		ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());
		getDriver().switchTo().window(tabs.get(1));
		return new TaleoResultsPO();
	}
	
}
