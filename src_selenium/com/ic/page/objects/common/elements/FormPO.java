package com.ic.page.objects.common.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.ic.selenium.ICSeleniumDriver;
import com.ic.utils.Utils;

public class FormPO extends ICSeleniumDriver{
	
	WebElement we = null;
	
	private WebElement findFontByClass(String value) {
		
		return findElementByAttribute("font","class", value);
	}
	
	private WebElement findElementByAttribute(String element, String attribute, String value) {
		
		we = getDriver().findElement(By.xpath("//" + element + 
				"[@" + attribute + "=" + Utils.enclose(value) + "]"));
		
		return we;
	}
	
	public String getErrorMsg() {

		WebElement fontTag = new FormPO().findFontByClass("errorMsg");
		return fontTag != null ? fontTag.getText() : null;
	}

	public String getSuccessMsg() {

		WebElement fontTag = new FormPO().findFontByClass("successMsg");
		return fontTag != null ? fontTag.getText() : null;

	}
	
}
