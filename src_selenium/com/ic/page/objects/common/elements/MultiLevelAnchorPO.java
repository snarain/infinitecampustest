package com.ic.page.objects.common.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.ic.utils.Utils;

public class MultiLevelAnchorPO extends FormPO{
	
	WebElement firstLevelElement;
	WebElement secondLevelElement;
	
	public MultiLevelAnchorPO(String firstLevel, String SecondLevel) {
		
		String parentMenuXpath = "//a[contains(text(),"+ Utils.enclose("Discussion")+")]";
		String subMenuXPath = "//a[contains(text(),"+ Utils.enclose("New Discussion")+")]";
		
		WebDriver wd = getDriver();
		firstLevelElement = wd.findElement(By.xpath(parentMenuXpath));
		secondLevelElement = wd.findElement(By.xpath(subMenuXPath));
	}
	
	public void click() {
		WebDriver wd = getDriver();
		Actions action = new Actions(wd);
		action.moveToElement(firstLevelElement).moveToElement(secondLevelElement).click().build().perform();	
		
	}

}
