package com.ic.page.objects.common.elements;

import org.openqa.selenium.WebElement;

import com.ic.utils.Utils;
import com.ic.page.objects.common.Xpath;

public class AnchorPO extends FormPO{
	
	String firstLevel;
	String secondLevel;
	WebElement anchor = null;

	public AnchorPO(String exactMatch) {
		
		String anchorXpath = "//a[normalize-space(text())="+ Utils.enclose(exactMatch)+"]";
		anchor = findElementByXpath(new Xpath(anchorXpath));
	}
	
	public AnchorPO(Xpath xpath) {
		
		anchor = findElementByXpath(xpath);
	}
	
	
	public void click() {
		anchor.click();
	}
	
	public String getValue() {
		return anchor.isDisplayed() ? anchor.getText() : null;
	}
	
	public boolean isExists() {
		return anchor != null;
	}

}
