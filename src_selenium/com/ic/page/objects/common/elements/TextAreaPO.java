package com.ic.page.objects.common.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class TextAreaPO extends FormPO {
	
	WebElement we;
	
	public TextAreaPO(String id) {
		
		we = findElementByID(id);
	}
	
	public boolean isExists() {
		return we != null;
	}
	
	public boolean isEnabled() {
		return we.isEnabled();
	}
	
	public void enterText(String _text) {
		we.sendKeys(_text);
	}
	
	public String getValue() {
		return we.getText();
	}
	
	

}
