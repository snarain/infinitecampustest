package com.ic.page.objects.common.elements;

import org.openqa.selenium.WebElement;

import com.ic.exceptions.ICSeleniumException;
import com.ic.page.objects.common.Xpath;

public class ButtonPO extends FormPO{
	
	WebElement button;
	String id;
	Xpath xpath;

	public ButtonPO(String id) {
		this.id = id;
		button = findElementByID(id);
	}
	
	public ButtonPO(Xpath xpath) {
		this.xpath = xpath;
		button = findElementByXpath(xpath);
	}

	public boolean isExists() {
		return button != null;
	}
	
	public boolean isVisible() {
		return button.isDisplayed();
	}
	
	
	

	public boolean isEnabled() {
		
		return button != null ? button.isEnabled(): false;
	}

	public void click() {
		
		click(false);
	}
	
	public void click(boolean confirmation) {
		
		if (button != null) {
			button.click();
			if(confirmation) {
				getDriver().switchTo().alert().accept();
			}
		}
		else {
			throw new ICSeleniumException("Button not found with id" + id);
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}



}
