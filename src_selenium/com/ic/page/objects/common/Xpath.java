package com.ic.page.objects.common;

import com.ic.utils.Utils;

public class Xpath {
	
	private String xpathStr;

	
	public Xpath(String type, String attr, String attrValue) {
		
		xpathStr = "//" + type + "[@" + attr + "=" + Utils.enclose(attrValue) + "]";
	}
	
	public Xpath(String xpathStr) {
		this.xpathStr = xpathStr;
	}

	/**
	 * @return the xpathStr
	 */
	public String getXpathStr() {
		return xpathStr;
	}


}
